import React, { useMemo, useState } from 'react'
import {
    Button,
    Checkbox,
    createStyles,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    makeStyles,
    Select,
    Typography,
} from '@material-ui/core'
import { KeyboardDatePicker } from '@material-ui/pickers'
import { TextField } from '@material-ui/core'
import dayjs, { Dayjs } from 'dayjs'
import { ICustomer, MembershipTierList, TMembershipTier } from '../store/data'

interface CustomerAddDialogProps {
    open: boolean
    onClose: () => void
    onAdd: (customer: ICustomer) => void
    customerList: ICustomer[]
}

const defaultProps: CustomerAddDialogProps = {
    open: true,
    onClose: () => {},
    onAdd: () => {},
    customerList: [],
}

const styles = makeStyles(() =>
    createStyles({
        formControl: {
            width: '100%',
        },
    })
)

export const CustomerAddDialog: React.FC<CustomerAddDialogProps> = (props) => {
    const { onClose, onAdd, open, customerList } = {
        ...defaultProps,
        ...props,
    }

    const classes = styles()

    const newID = useMemo(() => {
        let maxId = 0
        customerList.forEach((customer) => {
            if (customer.id > maxId) {
                maxId = customer.id
            }
        })
        return maxId + 1
    }, [customerList])
    const [birthDate, setBirthDate] = useState<Dayjs>(dayjs(new Date()))
    const [expiryDate, setExpiryDate] = useState<Dayjs>(dayjs(new Date()))
    const [firstName, setFirstName] = useState<string>('')
    const [firstNameError, setFirstNameError] = useState('')
    const [lastName, setLastName] = useState<string>('')
    const [lastNameError, setLastNameError] = useState('')
    const [customerActive, setCustomerActive] = useState<boolean>(true)
    const [memberShipTier, setMemberShipTier] = useState<TMembershipTier>(
        'Bronze'
    )

    const validate = (): boolean => {
        let isValid = true

        if (!firstName) {
            isValid = false
            setFirstNameError('first name required')
        } else {
            setFirstNameError('')
        }

        if (!lastName) {
            isValid = false
            setLastNameError('last name required')
        } else {
            setLastNameError('')
        }

        return isValid
    }

    const handleAdd = () => {
        if (validate()) {
            onAdd({
                id: 0,
                dob: birthDate,
                membershipExpires: expiryDate,
                membershipTier: memberShipTier,
                firstName,
                lastName,
            })
        }
    }

    const handleBirthDateChange = (dayjs: Dayjs | null) => {
        if (dayjs) setBirthDate(dayjs)
    }

    const handleExpiryDateChange = (dayjs: Dayjs | null) => {
        if (dayjs) setExpiryDate(dayjs)
    }

    return (
        <Dialog open={open} onClose={onClose} maxWidth={'xs'}>
            <DialogTitle>Add Customer</DialogTitle>
            <DialogContent>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            required
                            error={firstNameError !== ''}
                            helperText={firstNameError}
                            variant={'filled'}
                            label={'first name'}
                            value={firstName}
                            fullWidth
                            onChange={(e) => setFirstName(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            error={lastNameError !== ''}
                            helperText={lastNameError}
                            variant={'filled'}
                            label={'last name'}
                            value={lastName}
                            fullWidth
                            onChange={(e) => setLastName(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl
                            variant="filled"
                            className={classes.formControl}
                        >
                            <InputLabel htmlFor="age-native-simple">
                                Membership Tier
                            </InputLabel>
                            <Select
                                native
                                value={memberShipTier}
                                onChange={(e) =>
                                    setMemberShipTier(
                                        e.target.value as TMembershipTier
                                    )
                                }
                                inputProps={{
                                    name: 'membership tier',
                                    id: 'membership-tier',
                                }}
                            >
                                {MembershipTierList.map((tier) => {
                                    return <option value={tier}>{tier}</option>
                                })}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="YYYY-MM-DD"
                            margin="normal"
                            id="date-of-birth"
                            label="Date of birth"
                            value={birthDate}
                            onChange={handleBirthDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="YYYY-MM-DD"
                            margin="normal"
                            id="date-of-expiry"
                            label="Expiry Date"
                            value={expiryDate}
                            onChange={handleExpiryDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <FormControlLabel
                            labelPlacement="start"
                            control={
                                <Checkbox
                                    checked={customerActive}
                                    onChange={(e) =>
                                        setCustomerActive(e.target.checked)
                                    }
                                    name="customer active"
                                />
                            }
                            label="Customer Active"
                        />
                    </Grid>
                    <Grid
                        item
                        container
                        justify={'flex-end'}
                        alignContent={'flex-end'}
                        xs={6}
                    >
                        <Typography variant={'caption'}>id: {newID}</Typography>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button
                    fullWidth
                    onClick={() => onClose()}
                    variant={'contained'}
                    color={'secondary'}
                >
                    Close
                </Button>
                <Button
                    fullWidth
                    variant={'contained'}
                    color={'primary'}
                    onClick={() => handleAdd()}
                >
                    Add
                </Button>
            </DialogActions>
        </Dialog>
    )
}
