import React from 'react'
import styles from './CustomerTable.module.css'
import dayjs from 'dayjs'
import SortButton from './SortButton'
import {isActive, sortCustomers} from '../store/data-sort-utils'
import {LS_KEY_SORT_COLUMN_ID, LS_KEY_SORT_DIRECTION, useLocalStorageJson,} from '../store/localStorageJson'
import {ICustomer} from '../store/data';


export type TSortDirection = 'asc' | 'desc' | 'none'
export type SortButtonClickHandler = (columnId: string, sortDirection: TSortDirection) => void

function makeSortButton(columnId: string, currentSortColumnId: string,
                        sortDirection: TSortDirection, onButtonClick: SortButtonClickHandler) {

    const isSortButtonSelected = () => {
        return currentSortColumnId === columnId
    }

    return (
        <SortButton
            columnId={columnId}
            currentSortDirection={isSortButtonSelected() ? sortDirection : 'none'}
            onButtonClick={onButtonClick}
        />
    )
}

interface CustomerTableProps {
    customers: ICustomer[]
}

const CustomerTable: React.FC<CustomerTableProps> = ({customers}) => {
    const [currentSortColumnId, setSortColumnId] = useLocalStorageJson(
        LS_KEY_SORT_COLUMN_ID,
        'none'
    )

    const [sortDirection, setSortDirection] = useLocalStorageJson(
        LS_KEY_SORT_DIRECTION,
        'none'
    )

    const handleSortButtonClick = (columnId: string, newSortDirection: TSortDirection) => {
        setSortColumnId(newSortDirection === 'none' ? 'none' : columnId)
        setSortDirection(newSortDirection)
    }

    const sortedCustomers = sortCustomers(
        customers,
        currentSortColumnId,
        sortDirection
    ) as ICustomer[]

    return (
        <table className={styles.table}>
            <thead>
            <tr>
                <th colSpan={4} className={styles.borderRight}>
                    Personal details
                </th>
                <th colSpan={3}>Membership details</th>
            </tr>
            <tr>
                <th>
                    ID{' '}
                    {makeSortButton(
                        'id',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
                <th>
                    First name{' '}
                    {makeSortButton(
                        'firstName',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
                <th>
                    Last name{' '}
                    {makeSortButton(
                        'lastName',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
                <th className={styles.borderRight}>
                    Date of birth{' '}
                    {makeSortButton(
                        'dob',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
                <th>
                    Active?{' '}
                    {makeSortButton(
                        'isActive',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
                <th>
                    Tier{' '}
                    {makeSortButton(
                        'membershipTier',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
                <th>
                    Expiry date{' '}
                    {makeSortButton(
                        'membershipExpires',
                        currentSortColumnId,
                        sortDirection,
                        handleSortButtonClick
                    )}
                </th>
            </tr>
            </thead>
            <tbody>
            {sortedCustomers.map((customer) => (
                <tr
                    key={customer.id}
                    className={
                        isActive(customer) ? styles.active : styles.inactive
                    }
                >
                    <td>{customer.id}</td>
                    <td>{customer.firstName}</td>
                    <td>{customer.lastName}</td>
                    <td className={styles.borderRight}>
                        {dayjs(customer.dob).format('ll')}
                    </td>
                    <td>{isActive(customer) ? 'Yes' : 'No'}</td>
                    <td>{customer.membershipTier}</td>
                    <td>{dayjs(customer.membershipExpires).calendar()}</td>
                </tr>
            ))}
            </tbody>
        </table>
    )
}

export default CustomerTable
