import React from 'react'
import styles from './SortButton.module.css'
import {SortButtonClickHandler, TSortDirection} from './CustomerTable';

interface SortButtonProps {
    columnId: string
    currentSortDirection: TSortDirection
    onButtonClick: SortButtonClickHandler
}

const SortButton: React.FC<SortButtonProps> = ({columnId, currentSortDirection, onButtonClick}) => {
    let text, reminder

    switch (currentSortDirection) {
        case 'asc':
            text = '▲'
            reminder =
                'Sorting in ascending order. Click to sort in descending order.'
            break
        case 'desc':
            text = '▼'
            reminder = 'Sorting in descending order. Click to remove sort.'
            break
        default:
            text = '⬍'
            reminder = 'Not sorting. Click to sort in ascending order.'
            break
    }

    const toggleSortDirection = (currentSortDirection: TSortDirection) => {
        if (currentSortDirection === 'asc') {
            return 'desc'
        } else if (currentSortDirection === 'desc') {
            return 'none'
        } else {
            return 'asc'
        }
    }

    return (
        <span
            title={reminder}
            className={styles.sortButton}
            onClick={() => {
                onButtonClick(
                    columnId,
                    toggleSortDirection(currentSortDirection)
                )
            }}
        >
            {text}
        </span>
    )
}

export default SortButton