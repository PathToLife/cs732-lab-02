import styles from './NavBar.module.css'
import { NavLink } from 'react-router-dom'
import { useContext, useMemo } from 'react'
import { AppContext } from '../store/appContext'

export default function NavBar() {
    const { customerList } = useContext(AppContext)

    const {
        bronzeCount,
        silverCount,
        goldCount,
        platCount,
        total,
    } = useMemo(() => {
        const counts = {
            bronzeCount: 0,
            silverCount: 0,
            goldCount: 0,
            platCount: 0,
            total: 0,
        }

        customerList.forEach((customer) => {
            switch (customer.membershipTier) {
                case 'Bronze':
                    counts.bronzeCount += 1
                    break
                case 'Silver':
                    counts.silverCount += 1
                    break
                case 'Gold':
                    counts.goldCount += 1
                    break
                case 'Platinum':
                    counts.platCount += 1
                    break
            }
        })

        counts.total = customerList.length

        return counts
    }, [customerList])

    return (
        <div className={styles.navBar}>
            <NavLink to="/customers" activeClassName={styles.activeLink}>
                Customers
            </NavLink>
            <div className={styles.navBarDetails}>
                Customer details: Bronze: {bronzeCount}, Silver: {silverCount},
                Gold: {goldCount}, Platinum: {platCount}
                &nbsp;
                <strong>Total: </strong>
                {total}
            </div>
        </div>
    )
}
