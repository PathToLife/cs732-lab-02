import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { AppContext } from './store/appContext'
import NavBar from './components/NavBar'
import CustomersPage from './pages/CustomersPage'
import {
    LS_KEY_CUSTOMER_LIST,
    useLocalStorageJson,
} from './store/localStorageJson'
import { initialCustomers } from './store/data'

/**
 * Renders a navbar allowing the user to browse to the articles or gallery pages.
 * If the user tries to browse to any other URL, they are auto-redirected to the articles page.
 */
const App: React.FC = () => {
    const [customerList, setCustomerList] = useLocalStorageJson(
        LS_KEY_CUSTOMER_LIST,
        initialCustomers
    )

    return (
        <AppContext.Provider
            value={{
                customerList,
                setCustomerList,
            }}
        >
            <div className="container">
                <nav>
                    <NavBar />
                </nav>
                <Switch>
                    <Route path="/customers">
                        <CustomersPage />
                    </Route>
                    <Route path="*">
                        <Redirect to="/customers" />
                    </Route>
                </Switch>
            </div>
        </AppContext.Provider>
    )
}

export default App
