import { createContext } from 'react'
import { ICustomer } from './data'

interface IAppContext {
    customerList: ICustomer[]
    setCustomerList: (customerList: ICustomer[]) => void
}

export const AppContext = createContext<IAppContext>({
    customerList: [],
    setCustomerList: () => {},
})
