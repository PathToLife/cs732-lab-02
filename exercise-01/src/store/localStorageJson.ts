import { useState, useEffect } from 'react'

export const useLocalStorageJson = (key: string, initialValue: any = null) => {
    const [value, setValue] = useState(() => {
        // load the initial value from local storage if needed
        try {
            const value = localStorage.getItem(key)
            if (value !== null && value !== initialValue) {
                return value ? JSON.parse(value) : value
            } else {
                return initialValue
            }
        } catch (e) {
            console.error(
                `localStorage key:[${key}] failed to parse json value, returning initial state`
            )
            return initialValue
        }
    })

    useEffect(() => {
        if (value !== null) {
            localStorage.setItem(key, JSON.stringify(value))
        }
    }, [value, setValue, key])

    return [value, setValue]
}

// keys
export const LS_KEY_SORT_COLUMN_ID = 'LS_KEY_SORT_COLUMN_ID'
export const LS_KEY_SORT_DIRECTION = 'LS_KEY_SORT_DIRECTION'
export const LS_KEY_CUSTOMER_LIST = 'LS_KEY_CUSTOMER_LIST'
