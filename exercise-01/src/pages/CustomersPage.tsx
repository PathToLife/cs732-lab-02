import CustomerTable from '../components/CustomerTable'
import { Button, Grid, Paper, Box } from '@material-ui/core'
import { Route, useHistory } from 'react-router-dom'
import { CustomerAddDialog } from '../components/CustomerAddDialog'
import { useContext } from 'react'
import { AppContext } from '../store/appContext'
import { ICustomer } from '../store/data'

export default function CustomersPage() {
    const history = useHistory()

    const { setCustomerList, customerList } = useContext(AppContext)

    const handleCustomerAdd = (newCustomer: ICustomer) => {
        setCustomerList([...customerList, newCustomer])
        history.push('/customers')
    }

    return (
        <>
            <main>
                <Route path="/customers/add">
                    <CustomerAddDialog
                        open={true}
                        customerList={customerList}
                        onClose={() => history.goBack()}
                        onAdd={handleCustomerAdd}
                    />
                </Route>

                <Paper>
                    <Box p={2}>
                        <Grid container spacing={1} justify={'center'}>
                            <Grid item xs={12}>
                                <CustomerTable customers={customerList} />
                            </Grid>
                            <Grid item container xs={12} justify={'flex-end'}>
                                <Button
                                    variant={'contained'}
                                    color={'primary'}
                                    onClick={() =>
                                        history.push('/customers/add')
                                    }
                                >
                                    Add Customers
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Paper>
            </main>
        </>
    )
}
